/*=======================================================
 File name: Miniprojekt.c
 Author: Roman Grintsevich
 Compile with: Arduino IDE 1.6.13
 Date: 2016-11-15
 Description: Mini project in "Hårdvara och komponenter"
 Purpose: school exercise.
=======================================================*/

//LCD Display
#include <Wire.h> 
#include <LiquidCrystal_I2C.h> //LCD display library
LiquidCrystal_I2C lcd(0x27,20,4);  // set the LCD address to 0x27 for a 16 chars and 2 line display

//DS3231
#include <DS3231.h> //Time module library
DS3231 Clock;
bool Century=false;
bool h12;
bool PM;
byte ADay, AHour, AMinute, ASecond, ABits;
bool ADy, A12h, Apm;
int second,minute,hour,date,month,year,temperature;

unsigned int secondsPassed = 0, minutesPassed = 0, hoursPassed = 0, totalSecondsPassed = 0;
int startSeconds;

//Voltage divider
int analogInput = 0;
int sensor = 1;
int value;
float R1 = 99800.0;
float R2 = 47140.0;
float Vin = 0.0;
float Vout = 0.0;

float sensorReading, sensorVoltage, amps, power, energy;

void setup() 
{
  //Lcd display
  Wire.begin();
  lcd.init();        
  lcd.backlight();
  lcd.clear();
  
  //Voltage divider
  pinMode(analogInput, INPUT);

  //Current sensor
  pinMode(sensor, INPUT);

  //Set start time
  startSeconds = Clock.getSecond(); 
  
  //##DEBUG
  Serial.begin(9600);
}

void loop() 
{
  readDS3231(); //Read time and temperature from time module

  passedTime(); //Calculate passed time
  
  voltageInput(); //Read and calculate voltage
  readSensor(); //Read current sensor module
  
  displayMesuarements(); //Display measurments on the LCD display
  
  delay(1000);
}

//Read time module values (time and temprature)
void readDS3231()
{ 
  second=Clock.getSecond();
  minute=Clock.getMinute();
  hour=Clock.getHour(h12, PM);
  date=Clock.getDate();
  month=Clock.getMonth(Century);
  year=Clock.getYear();
  
  temperature=Clock.getTemperature();
}


//Display measurments on the LCD
void displayMesuarements()
{
  //Voltage
  lcd.setCursor(0,0);
  lcd.print("V:");
  lcd.print(Vin);
  lcd.print("V");

  //Amp
  lcd.setCursor(9,0);
  lcd.print("I:");
  lcd.print(amps);
  lcd.print("A");

  //Power
  lcd.setCursor(0,1); //P = U * I U - V I - A
  lcd.print("P:");
  power = Vin*amps;
  lcd.print(power);
  lcd.print("W");

  //Energy Wh
  lcd.setCursor(9,1);
  lcd.print("E:");
  energy = (Vin*amps*totalSecondsPassed)/3600;
  lcd.print(energy);
  lcd.print("Wh");

  //Energy kWh
  lcd.setCursor(9,2);
  lcd.print("E:");
  lcd.print(energy/1000);
  lcd.print("kWh");

  //Time m:s
  lcd.setCursor(0,2); 
  lcd.print("t:");
  lcd.print(minutesPassed);
  lcd.print(":");
  if(secondsPassed < 10)
  {
    lcd.print("0");
  }
  lcd.print(secondsPassed);

  //Temperatur
  lcd.setCursor(0,3); 
  lcd.print("T:");
  lcd.print(temperature);
  lcd.print("C");
}

//Voltage divider reading and calculation
void voltageInput()
{
  value = analogRead(analogInput); //Read analog input from voltage divider
  Vout = (value * 4.88)/1024.0;
  Vin = Vout / (R2 / (R1 + R2)); //Calculate Vin
}

//Calculate time passed
void passedTime()
{
  if(Clock.getSecond() < startSeconds)
    secondsPassed = Clock.getSecond() - startSeconds + 60;
  else
    secondsPassed = Clock.getSecond() - startSeconds;

  if(secondsPassed == 59)
  {
    minutesPassed++;
    if(minutesPassed == 59)
    {
      hoursPassed++;
      minutesPassed = 0;
    }
  }

  if(secondsPassed != 59 && minutesPassed != 59)
  {
    totalSecondsPassed = hoursPassed * 60 + minutesPassed * 60 + secondsPassed;
  }
}

//Current sensor reading and calculation
void readSensor()
{
  sensorReading = analogRead(sensor);
  sensorVoltage = (sensorReading / 1024.0) * 5000; //mV
  amps = ((sensorVoltage - 2500) / 185);
  
  if(amps < 0)
  {
    amps = 0;
  }
}
